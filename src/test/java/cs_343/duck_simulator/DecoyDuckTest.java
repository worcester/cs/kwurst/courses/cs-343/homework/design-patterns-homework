package cs_343.duck_simulator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

/**
 * Duck simulator based on Chapter 1 of Head First Design Patterns.
 * @author Karl R. Wurst
 * Copyright 2017 licensed under GPLv3
 *
 */
public class DecoyDuckTest {
	
	private Duck d1;
	private PrintStream oldOut;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private String newline = System.getProperty("line.separator");

	@BeforeEach
	public void setUp() {
		d1 = new DecoyDuck();
		oldOut = System.out;
		System.setOut(new PrintStream(outContent));
	}

	@Test
	public void testQuack() {
		d1.quack();
		assertEquals(outContent.toString(), "");
	}
	
	@Test
	public void testSwim() {
		d1.swim();		
		assertEquals(outContent.toString(), "Swim, swim" + newline);
	}
	
	@Test
	public void testDisplay() {
		d1.display();
		assertEquals(outContent.toString(), "I'm a Decoy Duck"+ newline);
	}
	
	@Test
	public void testFly() {
		d1.fly();
		assertEquals(outContent.toString(), "");
	}

	@AfterEach
	public void tearDown() {
		System.setOut(oldOut);
	}
}
