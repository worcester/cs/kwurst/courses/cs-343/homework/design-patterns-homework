package cs_343.duck_simulator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Duck simulator based on Chapter 1 of Head First Design Patterns.
 * @author Karl R. Wurst
 * Copyright 2017 licensed under GPLv3
 *
 */
public class RedHeadDuckTest {
	
	private Duck d1;
	private PrintStream oldOut;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private String newline = System.getProperty("line.separator");

	@BeforeEach
	public void setUp() {
		d1 = new RedHeadDuck();
		oldOut = System.out;
		System.setOut(new PrintStream(outContent));
	}

	@Test
	public void testQuack() {
		d1.quack();
		assertEquals(outContent.toString(), "Quack" + newline);
	}
	
	@Test
	public void testSwim() {
		d1.swim();
		assertEquals(outContent.toString(), "Swim, swim" + newline);
	}
	
	@Test
	public void testDisplay() {
		d1.display();
		assertEquals(outContent.toString(), "I'm a Red Head Duck" + newline);
	}
	
	@Test
	public void testFly() {
		d1.fly();
		assertEquals(outContent.toString(), "Flap, flap" + newline);
	}

	@AfterEach
	public void tearDown() {
		System.setOut(oldOut);
	}
}
