package cs_343.duck_simulator;

/**
 * Duck simulator based on Chapter 1 of Head First Design Patterns.
 * @author Karl R. Wurst
 * Copyright 2017 licensed under GPLv3
 *
 */
public abstract class Duck {
	
	public void quack() {
		System.out.println("Quack");
	}
	
	public void swim() {
		System.out.println("Swim, swim");
	}
	
	public abstract void display();
	
	public void fly() {
		System.out.println("Flap, flap");
	}

}
