package cs_343.duck_simulator;

/**
 * Duck simulator based on Chapter 1 of Head First Design Patterns.
 * @author Karl R. Wurst
 * Copyright 2017 licensed under GPLv3
 *
 */
public class DecoyDuck extends Duck {

	@Override
	public void quack() {
	}
	
	@Override
	public void display() {
		System.out.println("I'm a Decoy Duck");
	}
	
	@Override
	public void fly() {
	}

}
