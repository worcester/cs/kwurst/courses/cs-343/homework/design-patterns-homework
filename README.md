# Design Patterns Homework

**For the actual homework assignment, go to [DesignPatternsHomework.md](./DesignPatternsHomework.md).**

This is a poorly designed project that needs to be refactored using the Strategy Pattern, the Singleton Pattern, and the Simple Factory Pattern.

It has tests, and they all pass.

## Running and testing this project in Visual Studio Code

This project is set up to run in Visual Studio Code in Gitpod. This environment already has all the tools installed that you will need to do development.

1. The source code is in [src/main/java/cs_343/duck_simulator](src/main/java/cs_343/duck_simulator)
2. The test code is in [src/test/java/cs_343/duck_simulator](src/test/java/cs_343/duck_simulator)
3. To run the program, run the command `./gradlew run` in the terminal window.
4. To run the tests, run the command `./gradlew test` in the terminal window.

The code is also set up for Continuous Integration on GitLab.com. Whenever you push changes to GitLab.com the code will be built and the tests will run.

  1. If the code is being built or tests are being run, you will see a partially filled blue circle next to the commit.
  2. If the build and tests have completed and have all passed, you will see a green circle with a check mark next to the commit.
  3. If the build or the tests have completed with failures, you will see a red circle with an `x` next to the commit.
     1. Clicking on the red `x` will show you the pipeline stages, and whether the build has failed or whether the tests have failed.
     2. If the build has failed, you have syntax errors in your code. Click on the red `x` to see the compiler messages.
     3. If the tests have failed, click on the red `x` to see the test failure messages.

  **Note: It is faster and easier to run the build and tests in Visual Studio Code.**

&copy; 2024 Karl R. Wurst.

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
