# Homework &mdash; Intro to Design Patterns

## Objective

The objective of this assignment is for you to take a poorly designed program and refactor it to apply the Strategy Pattern, and create new strategies to apply to the client classes. For the Intermediate Add-On, you will refactor the two strategy class hierarchies to use the Singleton Pattern. For the Advanced Add-On you will refactor the DuckSimulator to use the Simple Factory Pattern.

## Instructions

You must fork the Design Patterns Homework repository to your private GitLab subgroup of the CS 343 01 02 Fall 2023 group. It will look something like 
`Worcester State University / Computer Science Department / CS 343 01 02 Fall 2022 / Students/ username`

Notes:

* The code as given implements the poorly designed version of the `DuckSimulator` with 4 subclasses of `Duck`, and with `fly()` and `quack()` overridden for `RubberDuck` and `DecoyDuck`.
* The code is set up as a Gradle project.

  1. The source code is in [src/main/java/cs_343/duck_simulator](src/main/java/cs_343/duck_simulator)
  2. The test code is in [src/test/java/cs_343/duck_simulator](src/test/java/cs_343/duck_simulator)
  3. To run the program, run the command `./gradlew run` in the terminal window.
  4. To run the tests, run the command `./gradlew test` in the terminal window.

* The code is also set up for Continuous Integration on GitLab.com. Whenever you push changes to GitLab.com the code will be built and the tests will run.
  1. If the tests are being run, you will see a partially filled blue circle next to the commit.
  2. If the tests have completed and have all passed, you will see a green circle with a check mark next to the commit.
  3. If the tests have completed with failures, you will see a red circle with an `x` next to the commit.
     1. Clicking on the red `x` will show you the pipeline stages, and whether the build has failed or whether the tests have failed.
     2. If the build has failed, you have syntax errors in your code. Click on the red `x` to see the compiler messages.
     3. If the tests have failed, click on the red `x` to see the test failure messages.

  **Note: It is faster and easier to run the build and tests in Visual Studio Code.**
   
* The 4 test classes will pass for the code as given.

### Original Design (as implemented)

```plantuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

class DuckSimulator {
   +{static}main(args : String[])
}
DuckSimulator -up-> MallardDuck
DuckSimulator -up-> RedHeadDuck
DuckSimulator -up-> RubberDuck
DuckSimulator -up-> DecoyDuck

abstract class Duck {
    +quack()
    +swim()
    +{abstract}display()
    +fly()
}

class MallardDuck extends Duck {
    +display()
}

class RedHeadDuck extends Duck {
    +display()
}

class DecoyDuck extends Duck {
    +quack()
    +display()
    +fly()
}
note right of DecoyDuck::quack
    overridden to do nothing
end note
note right of DecoyDuck::fly
    overridden to do nothing
end note

class RubberDuck extends Duck {
    +quack()
    +display()
    +fly()
}
note left of RubberDuck::quack
    overridden to squeak()
end note
note left of RubberDuck::fly
    overridden to do nothing
end note
```

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### Refactor the code to use the Strategy Pattern as in the design below:

```plantuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

class DuckSimulator {
   +{static}main(args : String[])
}
DuckSimulator -u-> MallardDuck
DuckSimulator -up-> RedHeadDuck
DuckSimulator -up-> RubberDuck
DuckSimulator -up-> DecoyDuck
DuckSimulator -up-> FlyWithWings
DuckSimulator -up-> FlyNoWay
DuckSimulator -up-> Quack
DuckSimulator -up-> Squeak
DuckSimulator -up-> MuteQuack

together {
    class Duck {
        -flyBehavior : FlyBehavior
        -quackBehavior : QuackBehavior
        +swim()
        +{abstract}display()
        +performFly()
        +performQuack()
        +setFlyBehavior(FlyBehavior)
        +setQuackBehavior(QuackBehavior)
    }

    class MallardDuck extends Duck {
        +display()
    }

    class RedHeadDuck extends Duck {
        +display()
    }

    class RubberDuck extends Duck {
        +display()
    }

    class DecoyDuck extends Duck {
        +display()
    }
}

    interface FlyBehavior <<Interface>> {
        +{abstract}fly()
    }

    class FlyWithWings implements FlyBehavior {
        +fly()
    }
    note bottom of FlyWithWings
        Implements 
        duck flying
        with wings.
    end note

    class FlyNoWay implements FlyBehavior {
        +fly()
    }
    note bottom of FlyNoWay
        Does
        nothing
    end note


    interface QuackBehavior <<Interface>> {
        +{abstract}quack()
    }
    class Quack implements QuackBehavior {
        +quack()
    }
    note bottom of Quack
        Quacking
        sound.
    end note

    class Squeak implements QuackBehavior {
        +quack()
    }
    note bottom of Squeak
        Squeaking
        sound.
    end note

    class MuteQuack implements QuackBehavior {
        +quack()
    }
    note bottom of MuteQuack
        No
        sound.
    end note


Duck *--- FlyBehavior
Duck *--- QuackBehavior
```

### Modify the Duck Simulator and the 4 Test Classes

In addition to the Strategy Pattern changes shown above, the `Duck Simulator` and the 4 test classes must be modified to use the appropriate `FlyBehavior` and `QuackBehavior` for each duck type.

* The only changes you should make in `DuckSimulator` are:
  * add lines to set the appropriate behaviors between the instantiation of each duck, and adding the duck to the pond.
  * ***Do not make changes anywhere else in the code.***
* The only changes you should make in the test classes are:
  * add lines to set the appropriate behaviors immediately after the instantiation of each duck in the `setUp()` method.
  * ***Do not make changes anywhere else in the code.***

#### Run The Tests

All four of the test cases should still pass.

### Add two new FlyBehaviors:

* `FlyWithRocket` - its `fly()` behavior should print `Blast Off`
* `FlyBeingThrown` - its `fly()` behavior should print `Please catch me...`

### Modify Duck Simulator, DecoyDuckTest, and RubberDuckTest to Assign the New FlyBehviors to Ducks

The `Duck Simulator` and the DecoyDuckTest and RubberDuckTest classes must be modified to 

* Assign `FlyWithRocket` to the `DecoyDuck`
* Assign `FlyBeingThrown` to the `RubberDuck`

* The only changes you should make in `DuckSimulator` are:
  * add lines to set the appropriate behaviors between the instantiation of each duck, and adding the duck to the pond. 
  * ***Do not make changes anywhere else in the code.***
* The only changes you should make in the test classes are:
  * add lines to set the appropriate behaviors immediately after the instantiation of each duck in the `setUp()` method. 
  * change the string to test for in the `testFly()` for `DecoyDuckTest` and `RubberDuckTest` 
  * ***Do not make changes anywhere else in the code.***

#### Run The Tests Again

All four of the test cases should still pass.

### Commit Your Working Code

Commit your working code with the commit message `Base Assignment` and push it to your fork.

## *Intermediate Add-On*

## Complete this portion in an Acceptable fashion to apply towards base course grades of C or higher

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

## Refactor to Use the Singleton Pattern

If you keep executing

```java
new FlyWithWings()
```

in your `main()` method when you create `Duck` objects and assigning a `FlyBehavior` to them, you are being wasteful. Since all `FlyWithWings` objects are exactly the same (they have no internal state - no attributes, and have only methods) there is no reason to make more than one of them.

If you create your `Duck` objects in only one place, then creating only one `FlyWithWings` object is simple. But since `Duck` objects could (theoretically) be created in multiple places in your code, it becomes harder to guarantee that only one `FlyWithWings` object exists...unless we let the `FlyWithWings` class guarantee that for us.

### Classic Singleton Pattern from Chapter 5 in *Head First Design Patterns, 2nd Edition*

Here is the [code for the Classic Singleton Pattern](https://github.com/bethrobson/Head-First-Design-Patterns/tree/master/src/headfirst/designpatterns/singleton/classic) from Chapter 5 in *Head First Design Patterns, 2nd Edition*

* [Singleton.java](https://github.com/bethrobson/Head-First-Design-Patterns/blob/master/src/headfirst/designpatterns/singleton/classic/Singleton.java) implements the singleton
* [SingletonClient.java](https://github.com/bethrobson/Head-First-Design-Patterns/blob/master/src/headfirst/designpatterns/singleton/classic/SingletonClient.java) uses the singleton to get a reference to the single instance.

#### Refactor the code to use the Singleton Pattern as in the design below:

```plantuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

class DuckSimulator {
   +{static}main(args : String[])
}
DuckSimulator -> Duck
DuckSimulator --> MallardDuck
DuckSimulator --> RedHeadDuck
DuckSimulator --> RubberDuck
DuckSimulator --> DecoyDuck
DuckSimulator --> FlyWithWings
DuckSimulator --> FlyNoWay
DuckSimulator --> Quack
DuckSimulator --> Squeak
DuckSimulator --> MuteQuack

together {
    class Duck {
        -flyBehavior : FlyBehavior
        -quackBehavior : QuackBehavior
        +swim()
        +{abstract}display()
        +performFly()
        +performQuack()
        +setFlyBehavior(FlyBehavior)
        +setQuackBehavior(QuackBehavior)
    }

    class MallardDuck extends Duck {
        +display()
    }

    class RedHeadDuck extends Duck {
        +display()
    }

    class RubberDuck extends Duck {
        +display()
    }

    class DecoyDuck extends Duck {
        +display()
    }
}

together {
    interface FlyBehavior <<Interface>> {
        +{abstract}fly()
    }

    class FlyWithWings implements FlyBehavior {
        -{static}instance : FlyBehavior
        +{static}getInstance() : FlyBehavior
        +fly()
        -FlyWithWings()
    }

    class FlyNoWay implements FlyBehavior {
        -{static}instance : FlyBehavior
        +{static}getInstance() : FlyBehavior
        +fly()
        -FlyNoWay()
    }
}

together {
    interface QuackBehavior <<Interface>> {
        +{abstract}quack()
    }
    class Quack implements QuackBehavior {
        -{static}instance : QuackBehavior
        +{static}getInstance() : QuackBehavior
        +quack()
        -Quack()
    }

    class Squeak implements QuackBehavior {
        -{static}instance : QuackBehavior
        +{static}getInstance() : QuackBehavior
        +quack()
        -Squeak()
    }

    class MuteQuack implements QuackBehavior {
        -{static}instance : QuackBehavior
        +{static}getInstance() : QuackBehavior
        +quack()
        -MuteQuack()
    }
}

Duck *--- FlyBehavior
Duck *--- QuackBehavior
```

***Note 1: You do not need to take into account multithreading behavior, so implement your Singleton pattern with the Classic Implementation. Do not implement the `synchronized` or `volatile` or `enum` versions of the Singleton.***

***Note 2: You will have two more `FlyBehavior` implementation classes than shown on the diagram above, after completing the Base Assignment.***

#### Modify the Duck Simulator and the 4 Test Classes (Intermediate)

The `Duck Simulator` and the 4 test classes must be modified to use the `getInstance()` method for the appropriate `FlyBehavior` and `QuackBehavior` for each duck type.

The only changes you should make are:

* The only changes you should make in `DuckSimulator` are:
  * modify the  lines to set the appropriate behaviors between the instantiation of each duck, and adding the duck to the pond.
  * ***Do not make changes anywhere else in the code.***
* The only changes you should make in the test classes are:
  * modify the lines to set the appropriate behaviors immediately after the instantiation of each duck in the `setUp()` method.
  * ***Do not make changes anywhere else in the code.***

#### Run The Tests (Intermediate)

All four of the test cases should still pass.

#### Commit Your Working Code (Intermediate)

Commit your working code with the commit message `Intermediate Add-On` and push it to your fork.

## *Advanced Add-On*

### Complete this portion in an Acceptable fashion to apply towards base course grades of B or higher

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

## Refactor to Use the Simple Factory Pattern

If you look back at the UML Class Diagram in the Intermediate Add-On, you will see that `DuckSimulator` is dependent on ***lots*** of other classes. Also, there are lots of details that you have to remember to build a single `Duck` object - like which behaviors each each type of duck needs.

And if we start creating `Duck` objects other places in our code, the problem gets more widespread! And just think about what happens if we start changing how a specific duck is created, or add new duck types!

Let's encapsulate all the details needed to create a duck of a specific type.

### Simple Factory Pattern from Chapter 4 in *Head First Design Patterns, 2nd Edition*

Here is the [code for Simple Factory Pattern](https://github.com/bethrobson/Head-First-Design-Patterns/tree/master/src/headfirst/designpatterns/factory/pizzas) from Chapter 4 in *Head First Design Patterns, 2nd Edition*

* [SimplePizzaFactory.java](https://github.com/bethrobson/Head-First-Design-Patterns/blob/master/src/headfirst/designpatterns/factory/pizzas/SimplePizzaFactory.java) creates different pizzas depending on pizza name you pass to it as a String. You will rewrite this to create different types of Ducks.
* The different pizza classes will be replaced with the Duck types you have already created. Have their constructors create the appropriate FlyBehavior and QuackBehavior for that type of Duck. Use the Singletons from the Intermediate Add-On.

## Refactor to Use the Simple Factory Pattern (Advanced)

```plantuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

class DuckSimulator {
   +{static}main(args : String[])
}
DuckSimulator --> Duck
DuckSimulator --> DuckFactory
DuckFactory --> MallardDuck
DuckFactory --> RedHeadDuck
DuckFactory --> RubberDuck
DuckFactory --> DecoyDuck

together {
    class Duck {
        -flyBehavior : FlyBehavior
        -quackBehavior : QuackBehavior
        +swim()
        +{abstract}display()
        +performFly()
        +performQuack()
        +setFlyBehavior(FlyBehavior)
        +setQuackBehavior(QuackBehavior)
    }

    class MallardDuck extends Duck {
        +display()
    }

    class RedHeadDuck extends Duck {
        +display()
    }

    class RubberDuck extends Duck {
        +display()
    }

    class DecoyDuck extends Duck {
        +display()
    }
}

abstract class DuckFactory {
   +{static}createDuck(duckType : String) : Duck
}
note top of DuckFactory: Use Simple Factory Pattern

together {
    interface FlyBehavior <<Interface>> {
        +{abstract}fly()
    }

    class FlyWithWings implements FlyBehavior {
        -{static}instance : FlyBehavior
        +{static}getInstance() : FlyBehavior
        +fly()
        -FlyWithWings()
    }

    class FlyNoWay implements FlyBehavior {
        -{static}instance : FlyBehavior
        +{static}getInstance() : FlyBehavior
        +fly()
        -FlyNoWay()
    }
}

together {
    interface QuackBehavior <<Interface>> {
        +{abstract}quack()
    }
    class Quack implements QuackBehavior {
        -{static}instance : QuackBehavior
        +{static}getInstance() : QuackBehavior
        +quack()
        -Quack()
    }

    class Squeak implements QuackBehavior {
        -{static}instance : QuackBehavior
        +{static}getInstance() : QuackBehavior
        +quack()
        -Squeak()
    }

    class MuteQuack implements QuackBehavior {
        -{static}instance : QuackBehavior
        +{static}getInstance() : QuackBehavior
        +quack()
        -MuteQuack()
    }
}

Duck *-right- FlyBehavior
Duck *-left- QuackBehavior
Duck --> DuckFactory
DuckFactory --> FlyWithWings
DuckFactory --> FlyNoWay
DuckFactory --> Quack
DuckFactory --> Squeak
DuckFactory --> MuteQuack
```

***Note: You will have two more `FlyBehavior` implementation classes than shown on the diagram above, after completing the Base Assignment.***

### Modify the Duck Simulator and the 4 Test Classes (Advanced)

The `Duck Simulator` and the 4 test classes must be modified to use the `DuckFactory` to create the ducks.

* The only changes you should make in `DuckSimulator` are:
  * modify the `main()` method to create each new `Duck` and add the it to the pond in a single statement. 
  * ***Do not make changes anywhere else in the code.***
* The only changes you should make in test classes are:
  * modify the `setUp()` method in each test to create the new Duck using the `DuckFactory`.
  * ***Do not make changes anywhere else in the code.***

#### Run The Tests (Advanced)

All four of the test cases should still pass.

#### Commit Your Working Code (Advanced)

Commit your working code with the commit message `Advanced Add-On` and push it to your fork.

### This is an Individual Assignment - No Partners

For this assignment, you will be working on your own, not with a partner. You should not be sharing your code with anyone else, other than the instructor.  

You will need to fork your own private Homework 2 repository to your private GitLab subgroup of the CS 343 01 Fall 2020 group for this project. The only person who should have any access to your repository is your instructor.  

You can ask questions on Discord about setting up your repository on GitLab, about using Git to send code to the instructor, and general questions about how to write your code. However you should not be posting sections of code and asking others to find your errors.

### Deliverables

For your Design PatternsHomework to be considered Meets Specification:

* Your code must compile, and the tests must pass.
* You must not have modified any of the test methods - only the setUp() method in each test class needs to be modified.
* You must have implemented your code to match the design diagrams given above, with matching class and method names.
* You must correctly capitalize your class names, method names, and variable names.
* You must indent consistently.
* You must use the appropriate commit messages for the base, intermediate add-on, and advanced add-on commits.  
* Your pushed commits should pass their tests when run by the GitLab CI runner.

The instructor will pull your Design Patterns Homework from your GitLab repository to grade it. Make sure:

1. You have pushed all changes to your private repository. (I can’t access local changes on your computer.)  

&copy; 2024 Karl R. Wurst.

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
